import requests
import threading
import time


_COOKIES = {'username': ''}

def send_message(frm, to, msg='', img=''):
    return requests.post('http://localhost:5000/message', data={
        'frm': frm,
        'to': to,
        'msg': msg,
        'img': img,
    },
    cookies=_COOKIES)

def get_messages_thread():
    while True:
        messages = requests.get('http://localhost:5000/get-messages', cookies=_COOKIES).json()
        for frm, msg, count in messages:
            print('\n{}({}): {}'.format(frm, count, msg))
        time.sleep(1)

if __name__ == '__main__':
    user = input('username: ')
    _COOKIES['username'] = user

    thread = threading.Thread(target=get_messages_thread)
    thread.start()

    while True:
        to = input('Send to: ')
        message = input('Message: ')
        send_message(user, to, message)
