import re

import requests
from flask import Flask
from flask import request
from flask import jsonify
from flask_socketio import SocketIO, send

from api.filters import accept_message


app = Flask(__name__)
socketio = SocketIO(app)

rooms = {}
users = {}

@socketio.on('join-room')
def join_room(data):
    print(data, request.sid)
    room = data['room']
    username = data['username']
    should_filter = data['should_filter']
    sid = request.sid
    users[sid] = {'username': username, 'filter': should_filter}

    if room not in rooms:
        rooms[room] = set()

    rooms[room].add((sid, username))

@socketio.on('leave-room')
def leave_room(data):
    print(data, request.sid)
    room = data['room']
    sid = request.sid

    rooms[room].remove((sid, users[sid]))

    if rooms[room] == set():
        del rooms[room]

@socketio.on('message')
def message(data):
    print(data, request.sid)
    frm = request.sid
    room = data['room']
    message = data['message']

    for c_sid, c_username in rooms[room]:
        if users[c_sid]['filter'] and not accept_message(message):
            print()
            continue

        socketio.emit('message', {
                'from': users[frm]['username'],
                'message': message,
                'room': room
            },
            room=c_sid,
            json=True
        )

if __name__ == '__main__':
    socketio.run(app, debug=True)
