# tesseract ocr extraction goes here

from PIL import Image
import pytesseract


def extract_text_from_image(img):
    """
    :param img:
        ``img`` is ``werkzeug.datastructures.FileStorage`` object.
    """
    path = img.stream.name
    img = Image.open(path)
    return pytesseract.image_to_string(img)
