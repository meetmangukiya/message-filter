import re

from api.ocr import extract_text_from_image


FILTERS = {
    'msg': set(),
    'img': set(),
}

def register_filter(typ):
    def decorator(func):
        FILTERS[typ].add(func)
        return func
    return decorator

def regex_generator(string):
    """
    Tries to generate a very simple regex for matching extended strings.

    Definition of extended strings:
        The characters in the string can appear for any number of times, and in
        same order.

    Example:
        goooooooodddddddddd mooooorrnnniiiinnnnggg is an extended string of good
        morning.
    """
    regex = r''
    whitespace = re.compile(r'\s')
    for char in string:
        if whitespace.match(char):
            regex += r'\s+'
        else:
            regex += r'{}+'.format(char)
    return re.compile(regex, re.IGNORECASE)

@register_filter('msg')
def simple_regex_filter(msg):
    """
    Returns True if any of the strings are present in the given message,
    indicating that the message should be filtered.
    """
    strings = [
        'good morning',
        'happy birthday',
        'hbd',
        'gm',
    ]

    festivals = [
        'Diwali',
        'Holi',
        'Dussehra',
        'Navratri',
        'Durga Puja',
        'Janmashtmi',
        'Ganesh Chaturthi',
        'Gurupurab',
        'Rakshabandhan',
        'Eid Ul Fitr',
        'Bihu',
        'Hemis',
        'Onam',
        'Pongal',
        'Christmas',
        'Easter',
        'Baisakhi',
    ]

    for festival in festivals:
        strings.append('happy {}'.format(festival))

    regexes = [regex_generator(string) for string in strings]

    for regex in regexes:
        if regex.match(msg):
            return True

    return False

@register_filter('img')
def simple_image_regex_filter(img):
    """
    :param img:
        ``img`` is a ``werkzeug.datastructures.FileStorage`` object.
    """
    text = extract_text_from_image(img)
    return simple_regex_filter(text)

def accept_message(msg=None, img=None):
    if msg:
        return not any(map(lambda x: x(msg), FILTERS['msg']))
    elif img:
        return not any(map(lambda x: x(img), FILTERS['img']))
